import {MethodNotAllowed} from 'http-errors'

export function allowedMethods (methods) {
  return (request, response, next) => {
    if (methods.includes(request.method)) next()
    else next(new MethodNotAllowed())
  }
}
