export function quietErrors () {
  return (error, request, response, next) => {
    const status = parseInt(error.status, 10) || 500
    const message = status >= 500
      ? 'Internal Server Error'
      : (error.message || 'Error')
    const headers = {
      'content-length': Buffer.byteLength(message),
      'content-type': 'text/plain'
    }
    if (!response.headersSent) {
      response.writeHead(status, message, headers)
    }
    if (!response.finished) {
      response.end(message)
    }
  }
}
