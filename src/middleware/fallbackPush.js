import debug from 'debug'
import {queue} from 'd3-queue'
import Cookies from 'cookies'
import {computeDigestValue} from 'cache-digest'
import {fileStream} from '../helpers/fileStream'
import {getPushPolicy} from '../helpers/getPushPolicy'
import {getDependencies} from '../helpers/getDependencies'
import {cacheDigestFilter} from '../helpers/cacheDigestFilter'
import {getOrigin} from '../helpers/getOrigin'
import {NotFound} from 'http-errors'

export function fallbackPush ({root, immutable, fallback, include, exclude, worker}) {
  const log = debug('http2server')
  return async (request, response, next) => {
    let headers
    let file
    try {
      ({headers, file} = await fileStream(request, fallback, {immutable}))
    } catch (error) {
      const problem = error.code === 'ENOENT' ? new NotFound() : error
      next(problem)
      return
    }
    const supportedPushPolicies = ['fast-load', 'default', 'head', 'none']
    const pushPolicy = getPushPolicy(request, supportedPushPolicies)
    if (pushPolicy) headers['push-policy'] = pushPolicy
    const doPush = pushPolicy !== 'none' // && !!response.push
    if (!doPush) {
      response.writeHead(200, headers)
      file.pipe(response)
      return
    }

    const baseUrl = getOrigin(request)
    const cookies = new Cookies(request, response)
    const pushQueue = queue(100)
    const pushPromises = []

    const dependencies = (await getDependencies({include, exclude, root, worker}))
      .filter(cacheDigestFilter(request, cookies, baseUrl))
      .filter(({absolute}) => absolute !== fallback)

    for (const dependency of dependencies) {
      try {
        const stream = await fileStream(request, dependency.absolute, {immutable})
        const pushStream = response.push(encodeURI(dependency.pathname), {
          status: 200,
          method: 'GET',
          request: {},
          response: stream.headers
        })
        if (pushStream) pushPromises.push({pushStream, dependency, stream})
        else break
      } catch (error) {
        console.error(error)
        continue
      }
    }

    if (request.headers['cache-response']) {
      if (cookies.get('cache-digest')) {
        cookies.set('cache-digest')
      }
    } else if (!cookies.get('cache-digest')) {
      const urls = pushPromises
        .filter(({stream: {isImmutable}}) => isImmutable)
        .map(({dependency: {pathname}}) => [baseUrl + pathname, null])
      const digestValue = computeDigestValue(false, urls, 2 ** 7)
      const cookie = Buffer.from(digestValue).toString('base64').replace(/=+$/, '')
      cookies.set('cache-digest', cookie)
    }

    response.writeHead(200, headers)
    file.pipe(response)

    for (const {pushStream, stream, dependency: {pathname}} of pushPromises) {
      pushQueue.defer(async (callback) => {
        try {
          // pushStream.writeHead(200, stream.headers)
          log(`PUSH ${pathname}`)
          if (pushPolicy === 'head') {
            pushStream.end()
            callback()
          } else {
            stream.file.pipe(pushStream)
            stream.file.on('error', () => {
              try {
                pushStream.stream.end()
              } catch (error) {
                console.error(error)
              }
              callback()
            })
            pushStream.on('error', (error) => {
              console.error(error)
              callback()
            })
            pushStream.on('finish', () => callback())
          }
        } catch (error) {
          console.error(error)
          callback()
        }
        return {
          abort: () => {
            try {
              pushStream.end()
            } catch (error) {
              console.error(error)
            }
          }
        }
      })
    }
  }
}
