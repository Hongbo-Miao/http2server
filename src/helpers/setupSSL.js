import {unlink, rmdir} from 'fs'
import {dirname} from 'path'
import {platform} from 'os'
import {exec} from 'child-process-promise'
import promisify from 'pify'
import shellescape from 'shell-escape'
import mkdirp from 'mkdirp'

async function generateLocalhostPair (key, cert) {
  const command = shellescape([
    'openssl', 'req',
    '-newkey', 'rsa:2048',
    '-new',
    '-nodes',
    '-sha256',
    '-x509',
    '-days', '365',
    '-keyout', key,
    '-out', cert,
    '-subj', '/CN=localhost'
  ])
  await exec(command)
  return
}

async function getDefaultKeychain () {
  const command = 'security default-keychain'
  const {stdout: keychain} = await exec(command)
  return keychain.trim().replace(/^"(.+)"$/, '$1')
}

async function addTrustedCertificate (keychain, cert) {
  const command = shellescape([
    'security',
    '-v', 'add-trusted-cert',
    '-r', 'trustRoot',
    '-p', 'ssl',
    '-k', keychain,
    cert
  ])
  await exec(command)
  return
}

export async function setupSSL (key, cert) {
  switch (platform()) {
    case 'darwin':
      try {
        await Promise.all([
          promisify(mkdirp)(dirname(key)),
          promisify(mkdirp)(dirname(cert))
        ])
        const keychain = await getDefaultKeychain()
        await generateLocalhostPair(key, cert)
        await addTrustedCertificate(keychain, cert)
      } catch (error) {
        try {
          await promisify(unlink)(key)
          await promisify(unlink)(cert)
          await promisify(rmdir)(dirname(key))
          await promisify(rmdir)(dirname(cert))
        } catch (error) {}
        throw new Error(`Failed to set up SSL certificate`)
      }
      break
    default:
      throw new Error('Generating certificates on this platform is not supported.')
  }
}
