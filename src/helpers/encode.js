import {createReadStream} from 'fs'
import {PassThrough} from 'stream'
import streamToPromise from 'stream-to-promise'
import {encoder} from './encoder'
import {EncodedFileCache} from './encodedFileCache'

const cache = new EncodedFileCache()

export async function encode (source, encoding) {
  let buffer
  if (cache.has(source, encoding)) {
    buffer = cache.get(source, encoding)
  } else {
    const input = createReadStream(source)
    const encoderStream = encoder(encoding)
    const encoderPromise = streamToPromise(encoderStream)
    input.on('error', (error) => {
      encoderStream.emit('error', error)
    })
    input.pipe(encoderStream)
    buffer = await encoderPromise
    cache.set(source, encoding, buffer)
  }

  const stream = new PassThrough()
  stream.end(buffer)

  return {
    stream,
    length: buffer.length
  }
}
