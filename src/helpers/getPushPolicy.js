function normalise (policy) {
  return policy
    .trim()
    .toLowerCase()
}

export function getPushPolicy (request, preferred) {
  const header = request.headers['accept-push-policy']
  const tokens = header ? header.split(',').map(normalise) : []
  const available = new Set(tokens)

  for (const policy of available) {
    if (preferred.includes(policy)) {
      return policy
    }
  }
}
