export const isHexRevved = new RegExp(
  '.+' + // Base filename
  '[-_.]' + // Separator
  '[0-9a-fA-F]+' + // Hexadecimal hash
  '(?:\\.\\w+)+$'// File extension(s)
)
