import mime from 'mime'
import {contentEncoding} from './contentEncoding'
import {encode} from './encode'
import {isEmojiRevved} from './isEmojiRevved'
import {isHexRevved} from './isHexRevved'

const patterns = Object.create(null)

function checkImmutable (filepath, immutable) {
  for (const pattern of immutable) {
    if (!(pattern in patterns)) {
      patterns[pattern] = pattern === 'emoji' ? isEmojiRevved
        : pattern === 'hex' ? isHexRevved
        : new RegExp(pattern)
    }
    if (patterns[pattern].test(filepath)) {
      return true
    }
  }
  return false
}

export async function fileStream (request, source, {immutable}) {
  const type = mime.lookup(source)
  const charset = mime.charsets.lookup(type)
  const encoding = contentEncoding(request, type, ['br', 'gzip', 'deflate'])

  const {stream: file, length} = await encode(source, encoding)
  const isImmutable = checkImmutable(source, immutable)

  const headers = {}
  headers['content-length'] = length
  headers['content-encoding'] = encoding
  headers['content-type'] = charset
    ? `${type}; charset=${charset}`
    : type
  headers['cache-control'] = isImmutable
    ? 'public, max-age=31536000, immutable'
    : 'public, must-revalidate'

  return {headers, file, isImmutable}
}
