import {statSync} from 'fs'

export function fileExists (path) {
  try {
    const stats = statSync(path)
    return stats.isFile()
  } catch (err) {
    return false
  }
}
