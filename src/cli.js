import {join, resolve} from 'path'
import {parse} from './argv'
import {master} from './master'
import debug from 'debug'
import opn from 'opn'
import {fileExists} from './helpers/fileExists'
import {directoryExists} from './helpers/directoryExists'
import {setupSSL} from './helpers/setupSSL'
import userHome from 'user-home'

async function main () {
  const argv = parse(process.argv.slice(2))

  process.title = 'http2server'

  if (!argv.silent) debug.enable(process.title)
  const log = debug(process.title)

  const publicDirectory = join(process.cwd(), 'public')
  argv.root = typeof argv._[0] !== 'undefined' ? resolve(argv._[0])
    : directoryExists(publicDirectory) ? publicDirectory
    : process.cwd()

  if (argv.key === 'key.pem' && argv.cert === 'cert.pem') {
    if (!fileExists(argv.key) && !fileExists(argv.cert)) {
      const key = join(userHome, '.' + process.title, argv.key)
      const cert = join(userHome, '.' + process.title, argv.cert)
      if (fileExists(key) && fileExists(cert)) {
        argv.key = key
        argv.cert = cert
      } else {
        log(`Generating a trusted SSL certificate for localhost development`)
        try {
          await setupSSL(key, cert)
          argv.key = key
          argv.cert = cert
        } catch (error) {
          console.error(error)
        }
      }
    }
  }

  argv.fallback = resolve(argv.root, argv.fallback)

  await master(argv)
  log(`Serving ${argv.root} on port ${argv.port}`)

  if (argv.o) opn(`https://localhost:${argv.port}/`)
}

main()
