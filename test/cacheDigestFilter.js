import test from 'ava'
import {cacheDigestFilter} from '../lib/helpers/cacheDigestFilter'

const fixtures = [
  {
    scenario: 'No digest present',
    request: {headers: {}},
    cookies: {get: () => undefined},
    baseUrl: 'https://example.com',
    given: [
      {pathname: '/style.css'}
    ],
    expected: [
      {pathname: '/style.css'}
    ]
  },

  {
    scenario: 'Digest present in header',
    request: {headers: {'cache-digest': 'AfdA; complete'}},
    cookies: {get: () => undefined},
    baseUrl: 'https://example.com',
    given: [
      {pathname: '/style.css'},
      {pathname: '/not_in_the_digest'}
    ],
    expected: [
      {pathname: '/not_in_the_digest'}
    ]
  },

  {
    scenario: 'Digest present in cookie',
    request: {headers: {}},
    cookies: {get: () => 'AfdA'},
    baseUrl: 'https://example.com',
    given: [
      {pathname: '/style.css'},
      {pathname: '/not_in_the_digest'}
    ],
    expected: [
      {pathname: '/not_in_the_digest'}
    ]
  },

  {
    scenario: 'Input should be unescaped URL',
    request: {headers: {'cache-digest': 'Aqqg; complete'}},
    cookies: {get: () => undefined},
    baseUrl: 'https://example.com',
    given: [
      {pathname: '/💩.js'},
      {pathname: decodeURI('/%F0%9F%92%A9.js')},
      {pathname: '/not_in_the_digest'}
    ],
    expected: [
      {pathname: '/not_in_the_digest'}
    ]
  }
]

for (const fixture of fixtures) {
  const {scenario, request, cookies, baseUrl, given, expected} = fixture
  test(scenario, (t) => {
    const actual = given.filter(
      cacheDigestFilter(request, cookies, baseUrl)
    )
    t.deepEqual(actual, expected)
  })
}
